Source: liblucy-perl
Section: perl
Priority: optional
Build-Depends: @cdbs@
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
Standards-Version: 3.9.6
Vcs-Git: git://anonscm.debian.org/pkg-perl/packages/liblucy-perl
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-perl/packages/liblucy-perl.git
Homepage: https://lucy.apache.org/

Package: liblucy-perl
Architecture: any
Depends: ${cdbs:Depends}, ${misc:Depends}, ${perl:Depends}, ${shlibs:Depends}
Recommends: ${cdbs:Recommends}
Description: search engine library
 The Apache Lucy search engine library delivers high-performance,
 modular full-text search.
 .
 Features:
  * Extremely fast.  A single machine can handle millions of documents.
  * Scalable to multiple machines.
  * Incremental indexing (addition/deletion of documents to/from an
    existing index).
  * Configurable near-real-time index updates.
  * Unicode support.
  * Support for boolean operators AND, OR, and AND NOT; parenthetical
    groupings; prepended +plus and -minus.
  * Algorithmic selection of relevant excerpts and highlighting of
    search terms within excerpts.
  * Highly customizable query and indexing APIs.
  * Customizable sorting.
  * Phrase matching.
  * Stemming.
  * Stoplists.
